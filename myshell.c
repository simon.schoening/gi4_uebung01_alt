#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <sys/wait.h>

int main(int argc, char* argv[], char* envp[]) 
{
    char* last_line;
    while(1){
        char cwd[1024]; //Buffer um current working dir zu speichern
        getcwd(cwd,1024);
        setenv("PWD", cwd, 1); //PWD-Variable aktuallisieren
        char* working_dir = (char*) malloc((strlen(cwd)+11)*sizeof(char)); //Working dir mit Formatierung für Farbe
        strcpy(working_dir, "\e[36m");
        strcat(working_dir, cwd);
        strcat(working_dir, "\e[0m$ ");
        last_line = readline(working_dir);

        int arg_count = 0;
        char* arg_pos = last_line;
        do{
            if(strcspn(arg_pos," ")>0)
                arg_count++;
            arg_pos = strchr(arg_pos,' ')+1;
        }while(arg_pos != NULL+1);

        if(arg_count==0) //Falls nichts eingegeben wurde, muss für die Zeile nichts gemacht werden.
        {
            free(last_line);
            continue;
        }
        add_history(last_line);

        char** args = (char**) malloc((arg_count+1) * sizeof(char*));
        args[0] = strtok(last_line," ");
        int i = 0;
        do
        {
            if(args[i][0] == '$')
                args[i] = getenv(args[i]+1);
            args[++i] = strtok(NULL," ");
        }
        while(i < arg_count);

        char* eq_ptr;

        if(strcmp("exit",args[0])==0)
        {
            free(last_line);
            break;
        }
        else if((eq_ptr = strchr(args[0],'='))!=NULL) //Zuweisung einer Variable der Form VARIABLE=WERT
        {
            *eq_ptr = '\0';
            setenv(args[0],eq_ptr+1,1);
        }
        else if (strcmp("cd",args[0])==0 || strcmp("chdir",args[0])==0) //cd
        {
            if(arg_count == 1)
            {
                chdir(getenv("HOME")); //Falls kein Parameter für cd gegeben ist, zum Home-Verzeichnis wechseln
            }
            else
                if(chdir(args[1])!=0)
                    printf("Das Verzeichnis konnte nicht geweschselt werden.\n");
        }
        else //Keine eingebaute Funktion der Shell, also wohl ein Programm
        {
            pid_t process_id = fork();
            if(process_id == 0)
            {
                if(execvp(args[0],args)==-1)
                {
                    printf("Kommando '%s' konnte nicht ausgeführt werden.\n",args[0]);
                    break; //Falls das Programm nicht gestartet werden kann, soll der Kindprozess beendet werden.
                }
            }
            else if(process_id < 0)
                printf("Fehler beim Erstellen des Kindsprozesses!\n");
            else
                waitpid(process_id,NULL,0); //Die Shell wartet, bis der gestartete Prozess beendet ist.
        }
        
        free(last_line);
    }
    return 0;
}
