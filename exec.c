#include <stdio.h>
#include <unistd.h>

int main(int argc, char* argv[], char* envp[]) 
{
  if(argc > 1)
  {
    execv(argv[1],argv+1);
    return 0;
  }
  else
  {
    printf("Kein Programmname angegeben!\n");
    return 1;
  }
}
