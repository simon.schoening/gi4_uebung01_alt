CC = gcc
CFLAGS = -Wall -g
LDLIBS = -lreadline 

.PHONY: all default clean

default: myecho myenv isset exec fork myshell

all: myecho myenv isset exec fork myshell

myecho: myecho.c

myenv: myenv.c

isset: isset.c

exec: exec.c

fork: fork.c

myshell: myshell.c

test: myecho myenv isset exec fork myshell
	./myecho
	./myenv
	./isset
	./exec
	./fork
	./myshell

clean:
	rm -f myecho myenv isset exec fork myshell
