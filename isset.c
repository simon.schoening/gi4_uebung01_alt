#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //Für getopt

extern char *optarg;

int main(int argc, char* argv[], char* envp[]) 
{
  char* env_ptr;

  if(argc==1)
    return 1; //Wenn keine Umgebungsvariable übergeben wird, kann sie auch nicht gesetzt sein.

  if(getopt(argc,argv,"v:")=='v') //Wurde "-v" benutzt?
  {
    env_ptr = getenv(optarg);
    printf("%s\n", ((env_ptr==NULL)?"Umgebungsvariable ist nicht gesetzt!":env_ptr));
  }
  else
    env_ptr = getenv(argv[1]); //Kein option char 'v'

  return (env_ptr==NULL)?1:0;
}
