#include <stdio.h>
#include <unistd.h>

int main(int argc, char* argv[], char* envp[]) 
{
  if(argc > 1)
  {
    pid_t process_id = fork();
    if(process_id == 0)
      execv(argv[1],argv+1);
    else if(process_id<0) //Fehler beim Erstellen des Kindsprozesses?
    {
      printf("Fehler beim Erstellen des Kindsprozesses!\n");
      return 2;
    }
    else
    {
      printf("Neuer Kindsprozess mit ID %d\n", process_id);
      return 0;
    }
  }
  else
  {
    printf("Kein Programmname angegeben!\n");
    return 1;
  }
}
